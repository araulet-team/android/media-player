package com.woodoop.android.devbed;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.woodoop.android.FPVideoAdListener;
import com.woodoop.android.FPVideoAdManager;

public class MainActivity extends ActionBarActivity {
    private final static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FPVideoAdManager fpManager = FPVideoAdManager.getInstance();
        fpManager.setContext(this);
        fpManager.setListener(new FPVideoAdListener() {
            @Override
            public void onCompletedVideoAd(boolean videoCompleted, boolean ctaClicked) {
                Log.d(TAG, "videoCompleted: " + videoCompleted);
                Log.d(TAG, "ctaClicked: " + ctaClicked);
            }
        });

        fpManager.showVideo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
