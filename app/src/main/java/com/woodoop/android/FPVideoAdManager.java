package com.woodoop.android;

import android.content.Context;
import android.util.Log;

import com.woodoop.android.mediaplayer.FPMediaPlayer;


public class FPVideoAdManager {
    private final static String TAG = "FPVideoAdManager";

    private static FPVideoAdManager fpManager;
    private Context context;
    private static FPVideoAdListener listener;

    public static FPVideoAdManager getInstance() {
        if(fpManager == null) {
            fpManager = new FPVideoAdManager();
        }

        return fpManager;
    }

    public void showVideo() {
        Log.d(TAG, "showVideo");
        FPMediaPlayer.getInstance().showVideo(context);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListener(FPVideoAdListener listener) {
        FPVideoAdManager.listener = listener;
    }
}
