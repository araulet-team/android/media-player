/**
 * Created by arnaudraulet on 15-09-19.
 */
package com.woodoop.android.mediaplayer;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import java.io.IOException;

// todo: do we need to catch an exception on getDuration?
// todo: make callback available describing video state
/*
 * 1. counter/mediaPlayerTimer on video ===> OK
 * 2. able to quit and comeback - video pause and play
 * 3. change orientation during playing ===> OK
 * 4. see compatibility
 * 5. could not launch more than one video
 *
 *
 * http://developer.android.com/reference/android/media/MediaPlayer.html
 * http://developer.android.com/guide/topics/media/mediaplayer.html
 */
public class FPMediaPlayerActivity extends Activity implements
        MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, SurfaceHolder.Callback{

    private final static String TAG = "MediaPlayerActivity";
    private final static String SOURCE_URL_VIDEO = "https://cdnapisec.kaltura.com/p/1855381/sp/185538100/playManifest/entryId/1_dfqscap6/flavorId/1_147bhe8l/format/url/protocol/https/a.mp4";

    private MediaPlayer player;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Handler mHandler = new Handler();

    private TextView mediaPlayerTimer;
    private FPVolumeReceiver keyReceiver;

    private boolean isHandlerRunning = false;
    private boolean isPlaying = false;
    private boolean isSurfaceViewCreated = false;

    /* events helper properties */
    private boolean isStartEventTrigger = false;
    private boolean isFirstQuartileEventTrigger = false;
    private boolean isThirdQuartileEventTrigger = false;
    private boolean isMidPointEventTrigger = false;

    /* video helper properties */
    private long videoCurrentPosition;
    private float videoPercentagePlayed;
    private long videoDuration;

    private boolean willEnterInForeground = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.media_player);

        mediaPlayerTimer = (TextView) findViewById(R.id.timer);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);

        registerVolumeNotification();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
        preserveAspectRatio();
    }

    private void registerVolumeNotification() {
        keyReceiver = new FPVolumeReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.media.VOLUME_CHANGED_ACTION");
        registerReceiver(keyReceiver, intentFilter);
    }

    /**
     * Media Player Wrapper Controls
     *  - start
     *  - pause
     *  - stop
     */
    public void start() {
        Log.d(TAG, "start called");

        if(player != null) {
            isPlaying = true;
            player.start();
            videoDuration =  player.getDuration();
        }

        if(!isHandlerRunning) {
            mHandler.post(startRunnableFPMediaPlayer);
        }
    }

    public void pause() {
        Log.d(TAG, "pause called");

        if(player != null) {
            isPlaying = false;
            player.pause();
        }
    }

    public void stop() {
        Log.d(TAG, "stop called");

        if(player != null) {
            isPlaying = false;
            player.reset();
            player.release();
            player = null;
        }
    }

    public void close() {
        stop();
        stopRunnableFPMediaPlayer();

        // avoid unregisterReceiver to throws IllegalArgumentException
        if(keyReceiver != null) {
            unregisterReceiver(keyReceiver);
            keyReceiver = null;
        }

        this.finish();
    }

    public void preserveAspectRatio() {
        int width = player.getVideoWidth();
        int height = player.getVideoHeight();

        float videoProportion = (float) width / (float) height;
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        float screenProportion = (float) size.x / (float) size.y;
        android.view.ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();

        if (videoProportion > screenProportion) {
            lp.width = size.x;
            lp.height = (int) ((float) size.x / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) size.y);
            lp.height = size.y;
        }

        surfaceView.setLayoutParams(lp);
    }

    /**
     * Media Player Runnable
     *
     * startRunnableFPMediaPlayer - Managing two tasks:
     *  - displaying timer (countdown) on the ui
     *  - triggering events
     *
     * stopRunnableFPMediaPlayer
     *  - cleaning runnable when leaving activity
     */
    private Runnable startRunnableFPMediaPlayer = new Runnable() {

        @Override
        public void run() {
            isHandlerRunning = true;
            startFPMediaPlayerTimer();
            startFPMediaPlayerEvents();
            mHandler.postDelayed(this, 500);
        }
    };

    private void startFPMediaPlayerTimer() {
        long currentDuration = player.getCurrentPosition();
        String timer = "" + countDownTimer(videoDuration, currentDuration);
        mediaPlayerTimer.setText(timer);
    }

    private int countDownTimer(long totalDuration, long currentDuration) {
        int current = (int) ((currentDuration % (1000*60*60)) % (1000*60) / 1000);
        int end = (int) ((totalDuration % (1000*60*60)) % (1000*60) / 1000);

        return end - current;
    }

    private void startFPMediaPlayerEvents() {
        videoCurrentPosition = player.getCurrentPosition();
        videoPercentagePlayed = (float) videoCurrentPosition / videoDuration * 100;

        if (!isStartEventTrigger && videoPercentagePlayed > 0) {
            trackEvent("start");
            isStartEventTrigger = true;
        } else if (!isFirstQuartileEventTrigger && videoPercentagePlayed > 25) {
            trackEvent("firstQuartile");
            isFirstQuartileEventTrigger = true;
        } else if (!isMidPointEventTrigger && videoPercentagePlayed > 50) {
            trackEvent("midpoint");
            isMidPointEventTrigger = true;
        } else if (!isThirdQuartileEventTrigger && videoPercentagePlayed > 75) {
            trackEvent("thirdQuartile");
            isThirdQuartileEventTrigger = true;
        }
    }

    private void stopRunnableFPMediaPlayer() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            isHandlerRunning = false;
        }
    }

    /**
     * surfaceView life cycle
     *
     * - surfaceCreated: call each time when Activity is created
     *
     *   -----------------
     *   - special cases -
     *   -----------------
     *      - home button clicked: surfaceDestroyed is called. User come back, surfaceCreated
     *        is called too.
     *      - power off clicked: surfaceDestroyed is not called. surfaceCreated is not called too
     *        when user come back.
     * */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "[Surface View] surfaceCreated");
        isSurfaceViewCreated = true;
        surfaceHolder = holder;

        if(!willEnterInForeground) {
            // calling stop() prevent creation of multiple instances
            stop();
            trackEvent("createView");
            buildMediaPlayer(holder);
            volumeDetection();
        } else {
            play();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "[Surface View] surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "[Surface View] surfaceDestroyed");
        isSurfaceViewCreated = false;
    }

    private void buildMediaPlayer(SurfaceHolder holder) {
        // 1 . idle state
        player = new MediaPlayer();
        player.setDisplay(holder);

        try {
            // 2. initialization state
            // player.setDataSource(MediaPlayerActivity.this, source);
            player.setDataSource(SOURCE_URL_VIDEO);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setVolume(1, 1);

        // 3. prepare state
        player.prepareAsync();

        // init listener
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnBufferingUpdateListener(this);
        player.setOnErrorListener(this);
    }

    private void play() {
        if(isSurfaceViewCreated) {
            player.setDisplay(surfaceHolder);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setVolume(1, 1);
            player.start();
        }
    }

    public void volumeDetection() {
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        switch( audio.getRingerMode() ){
            case AudioManager.RINGER_MODE_NORMAL:
                Log.d(TAG, "RINGER_MODE_NORMAL");
                trackEvent("unmute");
                break;
            case AudioManager.RINGER_MODE_SILENT:
                Log.d(TAG, "RINGER_MODE_SILENT");
                trackEvent("mute");
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                Log.d(TAG, "RINGER_MODE_VIBRATE");
                trackEvent("mute");
                break;
        }
    }

    /* media player events */
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.d(TAG, "[Media Player] onBufferingUpdate");
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "[Media Player] onCompletion");
        trackEvent("complete");
        close();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "[Media Player] onPrepared");
        start();
        preserveAspectRatio();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "[Media Player] onError=" + what + " extra=" + extra);
        trackEvent("error");
        close();
        return false;
    }

    /* remove back button behavior */
    @Override
    public void onBackPressed() {

    }

    /* android activity lifecycle */
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[Activity lifecycle] onStart");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[Activity lifecycle] onRestart");
        willEnterInForeground = true;
        play();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[Activity lifecycle] onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "[Activity lifecycle] onPause");
        pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[Activity lifecycle] onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[Activity lifecycle] onDestroy");
        close();
    }

    private void trackEvent(String eventName) {
        Log.e(TAG, "trackEvent with: " + eventName);
    }
}
