package com.woodoop.android.mediaplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by arnaudraulet on 15-12-04.
 */
public class FPVolumeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("FPVolumeReceiver", "volume is changing");
    }
}