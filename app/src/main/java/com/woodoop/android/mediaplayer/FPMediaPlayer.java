package com.woodoop.android.mediaplayer;


import android.content.Context;
import android.content.Intent;


public class FPMediaPlayer {

    private final static String TAG = "MediaPlayer";
    private static FPMediaPlayer instance;

    public static FPMediaPlayer getInstance() {
        if(instance == null) {
            instance = new FPMediaPlayer();
        }

        return instance;
    }



    public void showVideo(Context context) {
        Intent i = new Intent(context, FPMediaPlayerActivity.class);
        context.startActivity(i);
    }
}
