package com.woodoop.android.mediaplayer;

/**
 * Created by arnaudraulet on 15-11-24.
 */
public class FPMediaPlayerConfig {
    private final static String TAG = "FPMediaPlayerConfig";

    // all config that can be injected in the player for each video
    private FPMediaPlayerConfig fpMediaPlayerConfig;

    /* config player default settings */
    private boolean DEFAULT_SKIPPABLE_BEHAVIOUR = false;
    private boolean DEFAULT_REPLAY_BEHAVIOUR = false;
    private String DEFAULT_SKIPPABLE_OFFSET_TIMER = "3";

    /* config player */
    private boolean skippable = DEFAULT_SKIPPABLE_BEHAVIOUR;
    private boolean replay = DEFAULT_REPLAY_BEHAVIOUR;
    private String skippableOffsetTimer = DEFAULT_SKIPPABLE_OFFSET_TIMER;

    FPMediaPlayerConfig getInstance() {
        if(fpMediaPlayerConfig == null) {
            fpMediaPlayerConfig = new FPMediaPlayerConfig();
        }

        return fpMediaPlayerConfig;
    }

    public boolean isSkippable() {
        return skippable;
    }

    public void setSkippable(boolean skippable) {
        this.skippable = skippable;
    }

    public String getSkippableOffsetTimer() {
        return skippableOffsetTimer;
    }

    public void setSkippableOffsetTimer(String skippableOffsetTimer) {
        this.skippableOffsetTimer = skippableOffsetTimer;
    }

    public boolean isReplay() {
        return replay;
    }

    public void setReplay(boolean replay) {
        this.replay = replay;
    }
}
