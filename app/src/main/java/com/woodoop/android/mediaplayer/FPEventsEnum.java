package com.woodoop.android.mediaplayer;

/**
 * Created by arnaudraulet on 15-12-05.
 */
public enum FPEventsEnum {
    creativeView, // ok but need to be check if it's at the good place
    start, // ok
    midpoint,
    firstQuartile,
    thirdQuartile,
    complete,
    mute,
    unmute,
    pause,
    rewind,
    resume,
    fullscreen,
    expand,
    collapse,
    acceptInvitation,
    close
}
