package com.woodoop.android;

/**
 * Created by arnaudraulet on 15-12-07.
 */
public interface FPVideoAdListener {
    void onCompletedVideoAd(boolean videoCompleted, boolean ctaClicked);
}
